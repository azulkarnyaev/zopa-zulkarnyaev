The application calculates the best rate for requested loan from predefined lenders offers.

The lenders offers are rows of CSV file with lender's name, rate and available sum.

The application distributive consists from executable jar, application's source code and platform-specific 
scripts: `run.bat` for Windows and `run.sh` from Linux/MacOS. 

The first argument of the run scripts is the path to the file with offers. The second one
is the requested loan amount. For example

`./run.sh market.csv 1000`

If you want to build the application distributive, you should run Maven packaging task:

`mvn clean package`

After that `zopa-zulkarnyaev.zip` will be built in the `target` directory.