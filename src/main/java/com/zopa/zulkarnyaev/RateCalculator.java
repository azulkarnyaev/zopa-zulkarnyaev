package com.zopa.zulkarnyaev;

import ch.obermuhlner.math.big.BigDecimalMath;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;

public class RateCalculator {

    public static final BigDecimal MIN_VALUE = new BigDecimal(1000);
    public static final BigDecimal MAX_VALUE = new BigDecimal(15000);

    private final List<LenderOffer> sortedLenderOffers;

    public RateCalculator(List<LenderOffer> lenderOffers) {
        this.sortedLenderOffers = new ArrayList<>(lenderOffers);
        sortedLenderOffers.sort(Comparator.comparing(LenderOffer::getRate));
    }

    /**
     * Calculate the best rate for the loan.
     * <p>
     * The method can borrow money from several lenders and calculate the result that consists
     * from the base rate and repayments amount.
     * <p>
     * If borrowers don't have loan sum, then the method returns {@link Optional#empty()} result.
     *
     * @param loanSum loan amount. Must be greater than {@link #MIN_VALUE} pounds and less than {@link #MAX_VALUE}.
     * @param years   loan period. Must be greater than 0.
     * @return the calculation result with the best rate or {@link Optional#empty()},
     * if borrowers don't have the requested loan amount
     */
    public Optional<CalculationResult> calculateBestRate(BigDecimal loanSum, int years) {
        validateInputData(loanSum, years);
        Map<BigDecimal, BigDecimal> rateDistribution = getRateDistribution(loanSum);
        if (rateDistribution.isEmpty()) {
            return Optional.empty();
        }

        BigDecimal rate = calculateRate(rateDistribution, loanSum);

        BigDecimal monthlyRepayment = calculateMonthlyRepayment(rate, loanSum, years);
        BigDecimal totalRepayment = calculateTotalRepayment(monthlyRepayment, years);

        return Optional.of(
                new CalculationResult(
                        loanSum,
                        rate.multiply(BigDecimal.valueOf(100)).setScale(1, RoundingMode.HALF_UP),
                        monthlyRepayment.setScale(2, RoundingMode.HALF_UP),
                        totalRepayment.setScale(2, RoundingMode.HALF_UP))
        );
    }

    private void validateInputData(BigDecimal loanSum, int year) {
        if (year <= 0) {
            throw new IllegalArgumentException("Years amount must be greater than 0. Actual value: " + year);
        }
        if (loanSum.compareTo(MIN_VALUE) < 0 || loanSum.compareTo(MAX_VALUE) > 0) {
            throw new IllegalArgumentException(String.format("Loan amount must be between %s and %s",
                    MIN_VALUE, MAX_VALUE));
        }
    }

    private Map<BigDecimal, BigDecimal> getRateDistribution(BigDecimal loanSum) {
        BigDecimal remaining = new BigDecimal(loanSum.toString());
        Map<BigDecimal, BigDecimal> rateDistribution = new HashMap<>();
        for (LenderOffer offer : sortedLenderOffers) {
            if (offer.getAvailableSum().compareTo(remaining) > 0) {
                rateDistribution.put(offer.getRate(), remaining);
                remaining = BigDecimal.ZERO;
            } else {
                rateDistribution.put(offer.getRate(), offer.getAvailableSum());
                remaining = remaining.subtract(offer.getAvailableSum());
            }
        }
        if (remaining.equals(BigDecimal.ZERO)) {
            return rateDistribution;
        }
        return Collections.emptyMap();
    }

    private BigDecimal calculateRate(Map<BigDecimal, BigDecimal> rateDistribution,
                                     BigDecimal loanAmount) {
        return rateDistribution.entrySet().stream()
                .map(e -> e.getKey().multiply(e.getValue()))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(loanAmount, MathContext.DECIMAL128);
    }

    private BigDecimal calculateMonthlyRepayment(BigDecimal rate, BigDecimal loanAmount, int years) {

        // (1+rate)^(1/12)
        BigDecimal monthlyRate = BigDecimalMath.root(
                rate.add(BigDecimal.ONE), BigDecimal.valueOf(12),
                MathContext.DECIMAL128
        );

        // loan * (montlyRate - 1)/(1-1/(1+rate)^3)
        return loanAmount.multiply(monthlyRate.subtract(BigDecimal.ONE)
                .divide(
                        BigDecimal.ONE.subtract(
                                BigDecimal.ONE.divide(BigDecimal.ONE.add(rate).pow(years), MathContext.DECIMAL128)),
                        MathContext.DECIMAL128));
    }

    private BigDecimal calculateTotalRepayment(BigDecimal monthlyRepayment, int years) {
        return monthlyRepayment.multiply(BigDecimal.valueOf(years * 12));
    }

}
