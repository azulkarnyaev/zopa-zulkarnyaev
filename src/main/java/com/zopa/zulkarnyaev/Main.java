package com.zopa.zulkarnyaev;

import com.zopa.zulkarnyaev.exception.CsvParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("The application must have 2 input parameters");
        }
        String csvFileName = args[0];
        BigDecimal loanRequest;
        try {
            loanRequest = new BigDecimal(args[1]);

            List<LenderOffer> lenderOffers;
            lenderOffers = new CsvReader().readLenderOffers(csvFileName);
            Optional<CalculationResult> calculationResultOpt = new RateCalculator(lenderOffers)
                    .calculateBestRate(loanRequest, 3);

            if (calculationResultOpt.isPresent()) {

                DecimalFormat oneDigitFormat = new DecimalFormat();
                oneDigitFormat.setMaximumFractionDigits(1);

                DecimalFormat twoDigitFormat = new DecimalFormat();
                twoDigitFormat.setMaximumFractionDigits(2);

                CalculationResult calculationResult = calculationResultOpt.get();
                logger.info("Requested amount: £{}", calculationResult.getRequestedAmount());
                logger.info("Rate: {}%", oneDigitFormat.format(calculationResult.getRate()));
                logger.info("Monthly repayment: £{}", twoDigitFormat.format(calculationResult.getMonthlyRepayment()));
                logger.info("Total repayment: £{}", twoDigitFormat.format(calculationResult.getTotalRepayment()));
            } else {
                logger.info("It is not possible to provide a quote for requested sum");
            }

        } catch (NumberFormatException e) {
            logger.error("Can't parse loan amount " + args[1], e);
        } catch (CsvParseException e) {
            logger.error("Csv file parse error: ", e);
        } catch (IOException e) {
            logger.error("Failed to read csv file " + csvFileName, e);
        }

    }

}
