package com.zopa.zulkarnyaev;

import com.zopa.zulkarnyaev.exception.CsvParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CsvReader {

    public List<LenderOffer> readLenderOffers(String pathToCsv) throws IOException {
        try (Reader in = new FileReader(pathToCsv)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withFirstRecordAsHeader()
                    .parse(in);
            List<LenderOffer> lenderOffers = new ArrayList<>();
            for (CSVRecord record : records) {
                String lenderName = record.get("Lender");

                try {
                    BigDecimal rate = new BigDecimal(record.get("Rate"));
                    BigDecimal availableSum = new BigDecimal(record.get("Available"));

                    lenderOffers.add(new LenderOffer(lenderName, rate, availableSum));
                } catch (NumberFormatException e) {
                    throw new CsvParseException(String.format("Can't parse csv file content. File: %s, csv row: %s",
                            pathToCsv, record), e);
                }

            }
            return lenderOffers;
        }
    }

}
