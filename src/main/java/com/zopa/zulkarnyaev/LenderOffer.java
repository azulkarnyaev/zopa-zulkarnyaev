package com.zopa.zulkarnyaev;

import java.math.BigDecimal;
import java.util.Objects;

public class LenderOffer {

    private final String name;
    private final BigDecimal rate;
    private final BigDecimal availableSum;

    public LenderOffer(String name, BigDecimal rate, BigDecimal availableSum) {
        this.name = name;
        this.rate = rate;
        this.availableSum = availableSum;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getAvailableSum() {
        return availableSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LenderOffer that = (LenderOffer) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(rate, that.rate) &&
                Objects.equals(availableSum, that.availableSum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rate, availableSum);
    }

    @Override
    public String toString() {
        return "LenderOffer{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                ", availableSum=" + availableSum +
                '}';
    }

}
