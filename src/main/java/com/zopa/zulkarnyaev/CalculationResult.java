package com.zopa.zulkarnyaev;

import java.math.BigDecimal;
import java.util.Objects;

public class CalculationResult {

    private final BigDecimal requestedAmount;
    private final BigDecimal rate;
    private final BigDecimal monthlyRepayment;
    private final BigDecimal totalRepayment;

    public CalculationResult(BigDecimal requestedAmount,
                             BigDecimal rate,
                             BigDecimal monthlyRepayment,
                             BigDecimal totalRepayment) {
        this.requestedAmount = requestedAmount;
        this.rate = rate;
        this.monthlyRepayment = monthlyRepayment;
        this.totalRepayment = totalRepayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalculationResult that = (CalculationResult) o;
        return Objects.equals(requestedAmount, that.requestedAmount) &&
                Objects.equals(rate, that.rate) &&
                Objects.equals(monthlyRepayment, that.monthlyRepayment) &&
                Objects.equals(totalRepayment, that.totalRepayment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestedAmount, rate, monthlyRepayment, totalRepayment);
    }

    public BigDecimal getRequestedAmount() {
        return requestedAmount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getMonthlyRepayment() {
        return monthlyRepayment;
    }

    public BigDecimal getTotalRepayment() {
        return totalRepayment;
    }

}
