package com.zopa.zulkarnyaev;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RateCalculatorTest {

    @Test
    void calculateBestRate_oneOfferWithEnoughMoney() {
        RateCalculator rateCalculator = new RateCalculator(
                Collections.singletonList(
                        new LenderOffer("Alice", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000))
                )
        );
        Optional<CalculationResult> calculationResultOpt = rateCalculator.calculateBestRate(BigDecimal.valueOf(1000), 3);

        assertThat(calculationResultOpt.isPresent()).isTrue();

        CalculationResult calculationResult = calculationResultOpt.get();

        assertThat(calculationResult.getRequestedAmount()).isEqualTo(BigDecimal.valueOf(1000));
        assertThat(calculationResult.getRate()).isEqualTo(new BigDecimal("7.0").setScale(1));
        assertThat(calculationResult.getMonthlyRepayment()).isEqualTo(new BigDecimal("30.78"));
        assertThat(calculationResult.getTotalRepayment()).isEqualTo(new BigDecimal("1108.04"));
    }

    @Test
    void calculateBestRate_returnEmptyResult_ifLendersDontHaveMoneyEnough() {
        RateCalculator rateCalculator = new RateCalculator(
                Arrays.asList(
                        new LenderOffer("Alice", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000)),
                        new LenderOffer("Bob", BigDecimal.valueOf(0.05), BigDecimal.valueOf(800))
                )
        );

        Optional<CalculationResult> emptyResult = rateCalculator.calculateBestRate(BigDecimal.valueOf(10000), 3);
        assertThat(emptyResult.isPresent()).isFalse();
    }

    @Test
    void calculateBestRate_throwException_ifLenderOffersIsNull() {
        assertThatThrownBy(() -> new RateCalculator(null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void calculateBestRate_returnEmptyResult_whenEmptyInputData() {
        RateCalculator rateCalculator = new RateCalculator(Collections.emptyList());
        Optional<CalculationResult> calculationResult = rateCalculator.calculateBestRate(BigDecimal.valueOf(1000), 1);
        assertThat(calculationResult.isPresent()).isFalse();
    }

    @Test
    void calculateBestRate_throwException_whenLoanAmountGreaterThanMax() {
        RateCalculator rateCalculator = new RateCalculator(
                Collections.singletonList(
                        new LenderOffer("Alice", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000))
                )
        );
        assertThatThrownBy(() -> rateCalculator.calculateBestRate(RateCalculator.MAX_VALUE.add(BigDecimal.ONE), 1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void calculateBestRate_throwException_whenLoanAmountLessThanMin() {
        RateCalculator rateCalculator = new RateCalculator(
                Collections.singletonList(
                        new LenderOffer("Alice", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000))
                )
        );
        assertThatThrownBy(() -> rateCalculator.calculateBestRate(RateCalculator.MIN_VALUE.subtract(BigDecimal.ONE), 1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void calculateBestRate_calculateBestRate_whenUseSeveralLenderOffers() {
        RateCalculator rateCalculator = new RateCalculator(
                Arrays.asList(
                        new LenderOffer("Alice", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000)),
                        new LenderOffer("Bob", BigDecimal.valueOf(0.05), BigDecimal.valueOf(800)),
                        new LenderOffer("John", BigDecimal.valueOf(0.04), BigDecimal.valueOf(200))
                )
        );
        Optional<CalculationResult> calculationResultOpt = rateCalculator.calculateBestRate(BigDecimal.valueOf(1000), 3);

        assertThat(calculationResultOpt.isPresent()).isTrue();

        CalculationResult calculationResult = calculationResultOpt.get();

        assertThat(calculationResult.getRequestedAmount()).isEqualTo(BigDecimal.valueOf(1000));
        assertThat(calculationResult.getRate()).isEqualTo(new BigDecimal("4.8"));
        assertThat(calculationResult.getMonthlyRepayment()).isEqualTo(new BigDecimal("29.84"));
        assertThat(calculationResult.getTotalRepayment()).isEqualTo(new BigDecimal("1074.07"));
    }

}