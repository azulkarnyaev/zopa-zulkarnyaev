package com.zopa.zulkarnyaev;

import com.zopa.zulkarnyaev.exception.CsvParseException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CsvReaderTest {

    private CsvReader csvReader = new CsvReader();

    @Test
    void readLenderOffers_readSampleFile() throws IOException {
        List<LenderOffer> lenderOffers = readLenderOffers("market.csv");

        assertThat(lenderOffers).isNotNull();
        assertThat(lenderOffers.size()).isEqualTo(7);
        assertThat(lenderOffers).contains(new LenderOffer("Bob", new BigDecimal("0.075"),
                new BigDecimal(640)));
    }

    @Test
    void readLenderOffers_fileNotFound() {
        assertThatThrownBy(() -> csvReader.readLenderOffers("impossible_file_path.csv"))
                .isInstanceOf(FileNotFoundException.class);
    }

    @Test
    void readLenderOffers_corruptedFileHandling() {
        assertThatThrownBy(() -> readLenderOffers("corrupted_market.csv"))
                .isInstanceOf(CsvParseException.class);
    }

    private List<LenderOffer> readLenderOffers(String fileName) throws IOException {
        URL resource = getClass().getClassLoader().getResource(fileName);
        assertThat(resource).isNotNull();
        File file = new File(resource.getFile());

        return csvReader.readLenderOffers(file.getAbsolutePath());
    }

}
